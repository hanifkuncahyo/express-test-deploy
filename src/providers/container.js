//Init All Service
const db = require("../../models");
//Init All Service
const customerService = require("../service/customer-service")(db);
//Init All Controller
const customerController = require("../controller/customer-controller")(customerService);
//Init All Router
const customerRouter = require("../router/customer-router")(customerController);

module.exports = {
    customerRouter,
}