const express = require("express");
const container = require("./container");

const app = express();

app.get("/", (req, res) => { res.send("hore") })
app.use("/customers", container.customerRouter);

module.exports = app;