const app = require("../../src/providers/app");
const request = require("supertest");

describe('testing app module',() =>{
    test('route index page must return 200', () =>{
        return request(app).get('/').expect(200);
    });
})